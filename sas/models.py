from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.decorators import login_required, permission_required
from django.http import HttpResponseRedirect
from django.utils.translation import gettext_lazy as _
from django.core import serializers
import json
from django.db.models import F


# Create your models here.

class Profile(models.Model):
    web_verbose_name = "Participant"
    web_verbose_name_plural = "Participants"
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    ismonitor = models.BooleanField(default=False)
    birthdate = models.DateField(null=True, blank=True)
    address = models.CharField(max_length=500, null=True, blank=True)
    sessions = models.ManyToManyField('Session', blank=True)
    evals = models.ManyToManyField('Eval', blank=True)

    class Meta:
        verbose_name = 'Bénéficiaire'
        verbose_name_plural = 'Bénéficiaires'

    def first_name(self):
        return self.user.first_name

    def last_name(self):
        return self.user.last_name


    def __str__(self):
        return f"{self.user.last_name} {self.user.first_name}"

    def get_absolute_url(self):
        """Returns the url to access a detail record for this user."""
        return reverse('user-detail', args=[str(self.id)])

    def sorted_evals(self):
        sorted_evals = []
        for ses in self.sessions.all():
             evas = []
             for eva in self.evals.all():
                 if eva.session == ses:
                     evas.append(eva)
             sorted_evals.append({'session': ses, 'evals': evas})
        return sorted_evals



class Module(models.Model):
    web_verbose_name = "Module"
    web_verbose_name_plural = "Modules"
    name = models.CharField(max_length=200)
    summary = models.TextField(max_length = 200, help_text='Enter a brief description of the content of the module', null=True, blank=True)
    description = models.TextField(help_text='Enter a complete description of the content of the module', null=True, blank=True)
     
    class Meta:
        ordering = ['name']
        verbose_name = "Module"
        verbose_name_plural = "Modules"

    def __str__(self):
        return f'{self.name}'

    def get_absolute_url(self):
        """Returns the url to access a detail record for this module."""
        return reverse('module-detail', args=[str(self.id)])

    def get_edit_url(self):
        return reverse('module-update', args=[str(self.id)])

    def get_delete_url(self):
        return reverse('module-delete', args=[str(self.id)])

    def class_name(self):
        return self._meta.verbose_name

# A skill is something that you are able to achieve
class Skill(models.Model):
    web_verbose_name = "Compétence"
    web_verbose_name_plural = "Compétences"
    name = models.CharField(max_length=200, verbose_name="Nom")
    description = models.TextField(max_length=1000, null=True, blank=True, verbose_name="Description")

    modules = models.ManyToManyField(Module, related_name="skill_modules", verbose_name="Modules")


    # Difficulty is noted beetween 1 and 5 
    difficulty = models.CharField(
        verbose_name="Difficulté",
        max_length=1,
        default="3", 
        choices=[('1', "1"), ('2', "2"), ('3', "3"), ('4', "4"), ('5', "5")], 
     )

    #tools = ""
    #ressources = ""

    class Meta:
        ordering = ['name']
        verbose_name = 'Compétence'
        verbose_name_plural = 'Compétences'

    def __str__(self):
        return f"{self.name}"

    def get_absolute_url(self):
        return reverse('skill-detail', args=[str(self.id)])

    def get_edit_url(self):
        return reverse('skill-update', args=[str(self.id)])

    def get_delete_url(self):
        return reverse('skill-delete', args=[str(self.id)])

    def class_name(self):
        return self._meta.verbose_name

# A challenge is something to accomplish, in general terms, e.g. "install a computer"
class Challenge(models.Model):
    web_verbose_name = "Challenge"
    web_verbose_name_plural = "Challenges"
    name = models.CharField(max_length=200, null=True)
    description = models.TextField(max_length=1000)
 
    modules = models.ManyToManyField(Module, blank=True, related_name="challenge_modules", related_query_name="challenge_modules")
    challenges = models.ManyToManyField(self, blank=True, related_name="challenge_challenges", related_query_name="challenge_challenges")

    skills = models.ManyToManyField(Skill, blank=True, related_name="challenge_skills", related_query_name="Compétences")
    #get_all_skills_ids = MU.get_related_ids(self, skills, main=main_skill, far=modules)

    # Difficulty is noted beetween 1 and 5 
    difficulty = models.CharField(
        max_length=1,
        default="3", 
        choices=[('1', "1"), ('2', "2"), ('3', "3"), ('4', "4"), ('5', "5")], 
     )

    class Meta:
        ordering = ['name']
        verbose_name = 'Challenge'
        verbose_name_plural = 'Challenges'

    def __str__(self):
        return f"{self.name}"

    def get_absolute_url(self):
        return reverse('challenge-detail', args=[str(self.id)])

    def get_edit_url(self):
        return reverse('challenge-update', args=[str(self.id)])

    def get_delete_url(self):
        return reverse('challenge-delete', args=[str(self.id)])

    def class_name(self):
        return self._meta.verbose_name


# A mission is an instance of  challenge, e.g. "install a computer for x"
class Mission(models.Model):
    web_verbose_name = "Mission"
    web_verbose_name_plural = "Missions"
    challenge = models.ForeignKey(Challenge, on_delete=models.PROTECT, related_name='mission_challenge')
    context = models.TextField(max_length=1000, null=True)
    participants = models.ManyToManyField(User, blank=True, related_name="mission_participants")
    start_date = models.DateTimeField(default=timezone.now)
    end_date = models.DateTimeField(default=timezone.now)
    monitor = models.ForeignKey(User, on_delete=models.PROTECT, null=True, related_name="mission_monitor") # don't forget to set monitor to NULL is monitor is archived BEFORE challenge take place (issue #17)

    class Meta:
        ordering = ['context']
        verbose_name = 'Mission'
        verbose_name_plural = 'Missions'

    def __str__(self):
        return f"{self.context}"

    def get_absolute_url(self):
        return reverse('mission-detail', args=[str(self.id)])

    def get_edit_url(self):
        return reverse('mission-update', args=[str(self.id)])

    def get_delete_url(self):
        return reverse('mission-delete', args=[str(self.id)])

    def class_name(self):
        return self._meta.verbose_name


class Workshop(models.Model):
    web_verbose_name = "Workshop"
    web_verbose_name_plural = "Workshops"
    name = models.CharField(max_length=200, null=True)
    description = models.TextField(max_length=1000, null=True)
    start_date = models.DateTimeField(default=timezone.now)
    end_date = models.DateTimeField(default=timezone.now)

    # Difficulty is noted beetween 1 and 5 
    difficulty = models.CharField(
        max_length=1,
        default="3", 
        choices=[('1', "1"), ('2', "2"), ('3', "3"), ('4', "4"), ('5', "5")], 
    )

    challenges = models.ManyToManyField(Challenge, blank=True, related_name='workshop_challenges')

    modules = models.ManyToManyField(Module, blank=True, related_name='workshop_modules')

    skills = models.ManyToManyField(Skill, blank=True, related_name='workshop_skills', related_query_name="Compétences")

    monitor = models.ForeignKey(User, on_delete=models.PROTECT, null=True, related_name="workshop_monitor") # don't forget to set monitor to NULL is monitor is archived BEFORE workshop take place (issue #17)

    class  Meta:
        ordering = ['start_date', 'end_date']
        verbose_name_plural = 'Ateliers'
        verbose_name = 'Atelier'

    def __str__(self):
        return f"{self.name}"

    def get_absolute_url(self):
        return reverse('workshop-detail', args=[str(self.id)])

    def get_edit_url(self):
        return reverse('workshop-update', args=[str(self.id)])

    def get_delete_url(self):
        return reverse('workshop-delete', args=[str(self.id)])

    def class_name(self):
        return self._meta.verbose_name



class Partner(models.Model):
    web_verbose_name = "Partenaire"
    web_verbose_name_plural = "Partenaires"
    name = models.CharField(max_length=500)
    address = models.CharField(max_length=500, null=True, blank=True)
    intermediary = models.ForeignKey(User, null=True, on_delete=models.PROTECT, related_name="Intermédiaire")

    class Meta:
        ordering = ['name']
        verbose_name = "Partenaire"
        verbose_name_plural = "Partenaires"

    def __str__(self):
        return f"{self.name}"

    def get_absolute_url(self):
        return reverse('partner-detail', args=[str(self.id)])

    def get_edit_url(self):
        return reverse('partner-update', args=[str(self.id)])

    def get_delete_url(self):
        return reverse('partner-delete', args=[str(self.id)])

    def class_name(self):
        return self._meta.verbose_name



# A parcours is a convention beetween a student and the organization, along with a list of modules to evaluate and challenges to take
class Parcours(models.Model):
    web_verbose_name = "Parcours"
    web_verbose_name_plural = "Parcours"

    user = models.ForeignKey(User, null=True, on_delete=models.PROTECT, related_name="parcours_student")
    tiers = models.ForeignKey(Partner, null=True, on_delete=models.PROTECT, related_name="parcours_tiers")
    monitor = models.ForeignKey(User, on_delete=models.PROTECT, null=True, related_name="parcours_monitor")

    class Status(models.TextChoices):
        COLLEGE = 'CO', _('Stage au collège')
        LYCEE = 'LY', _('Stage au Lycée')
        SUPERIEUR = 'SUP', _("Stage d'Études Supérieures")
        FORMATION = 'FOR', _("Stagiaire de la formation professionnelle")
        AUTRE = 'AUT', _("Autre")

    type_of_convention = models.CharField(
        max_length=3,
        choices=Status.choices,
        default=Status.LYCEE,
    )

    other_partners = models.ManyToManyField(Partner, blank=True)
    start_date = models.DateTimeField(default=timezone.now)
    end_date = models.DateTimeField(default=timezone.now)

    modules =  models.ManyToManyField(Module, blank=True)

    challenges = models.ManyToManyField(Challenge, blank=True)


    class Meta:
        ordering = ['end_date', 'start_date']
        permissions = (("can_evaluate_student", "Évaluer les étudiants"),)
        verbose_name = "Parcours"
        verbose_name_plural = "Parcours"

    def __str__(self):
        return f"{self.type_of_convention} de {self.user.first_name} {self.user.last_name} "

    def get_absolute_url(self):
        return reverse('parcours-detail', args=[str(self.id)])

    def get_edit_url(self):
        return reverse('parcours-update', args=[str(self.id)])

    def get_delete_url(self):
        return reverse('parcours-delete', args=[str(self.id)])

    def class_name(self):
        return self._meta.verbose_name



class Session(models.Model):
    web_verbose_name = "Session"
    web_verbose_name_plural = "Sessions"
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    start_date = models.DateTimeField(default=timezone.now)
    end_date = models.DateTimeField(default=timezone.now)
    parcours = models.ForeignKey(Parcours, on_delete=models.SET_NULL, blank=True, null=True)
    module = models.ForeignKey(Module, on_delete=models.SET_NULL, blank=True, null=True)
    mission = models.ForeignKey(Mission, on_delete=models.SET_NULL, blank=True, null=True)
    workshop = models.ForeignKey(Workshop, on_delete=models.SET_NULL, blank=True, null=True)
    note = models.TextField(max_length=1000)

    class Meta:
        ordering = ['end_date', 'start_date']
        verbose_name = "Session"
        verbose_name_plural = "Sessions"

    def __str__(self):
        return f"session de {self.user})"

    def get_absolute_url(self):
        return reverse('session-detail', args=[str(self.id)])

    def get_edit_url(self):
        return reverse('session-update', args=[str(self.id)])

    def get_delete_url(self):
        return reverse('session-delete', args=[str(self.id)])

    def class_name(self):
        return self._meta.verbose_name



class Eval(models.Model):
    web_verbose_name = "Évaluation"
    web_verbose_name_plural = "Évaluations"
    student = models.ForeignKey(User, on_delete=models.PROTECT, related_name="éval_student")
    parcours = models.ForeignKey(Parcours, on_delete=models.PROTECT, related_name="éval_parcours")
    module = models.ForeignKey(Module, null=True, on_delete=models.PROTECT, related_name="éval_module")
    challenge = models.ForeignKey(Challenge, null=True, on_delete=models.PROTECT, related_name="éval_challenge")
    workshop = models.ForeignKey(Workshop, null=True, on_delete=models.PROTECT, related_name="éval_workshop")
    comment = models.TextField(max_length=1000)
    author = models.ForeignKey(User, null=True, on_delete=models.PROTECT, related_name="Évaluateur")

    class Meta:
        ordering = ['student', 'parcours']
        permissions = (("can_evaluate_student", "Évaluer les étudiants"),)
        verbose_name = "Évaluation"
        verbose_name_plural = "Évaluations"

    def __str__(self):
        return f"évaluation de {self.student.first_name} {self.student.last_name} pour {self.user}"

    def get_absolute_url(self):
        return reverse('eval-detail', args=[str(self.id)])

    def get_edit_url(self):
        return reverse('eval-update', args=[str(self.id)])

    def get_delete_url(self):
        return reverse('eval-delete', args=[str(self.id)])

    def class_name(self):
        return self._meta.verbose_name

