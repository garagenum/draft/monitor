from django.contrib import admin
from .models import Profile, Module, Skill, Challenge, Mission, Workshop, Session, Partner, Parcours, Eval
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

# Register your models here.

#@admin.register(User)
#class UserAdmin(admin.ModelAdmin):
#    list_display = ('last_name', 'first_name', 'birthdate')
#    list_filter = ['birthdate']

# Define an inline admin descriptor for Employee model
# which acts a bit like a singleton
class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name = 'Profil'

# Define a new User admin
class UserAdmin(BaseUserAdmin):
    inlines = (ProfileInline,)

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)

class SessionInline(admin.TabularInline):
    model = Session
    extra = 0

class ChallengeInline(admin.TabularInline):
    model = Challenge
    extra = 0

@admin.register(Module)
class ModuleAdmin(admin.ModelAdmin):
    list_display = ['name']
    inlines = [SessionInline] 

@admin.register(Skill)
class SkillAdmin(admin.ModelAdmin):
    pass

@admin.register(Challenge)
class ChallengeAdmin(admin.ModelAdmin):
    pass

@admin.register(Mission)
class MissionAdmin(admin.ModelAdmin):
    pass

@admin.register(Parcours)
class ParcoursAdmin(admin.ModelAdmin):
    pass

@admin.register(Partner)
class PartnerAdmin(admin.ModelAdmin):
    pass

@admin.register(Workshop)
class WorkshopAdmin(admin.ModelAdmin):
    pass

@admin.register(Session)
class SessionAdmin(admin.ModelAdmin):
    list_display = ('user', 'start_date', 'end_date', 'module', 'mission', 'workshop')
    list_filter = ('user', 'start_date', 'end_date', 'module', 'mission', 'workshop')
    fields = ['user', ('start_date', 'end_date'), 'module', 'mission', 'workshop']

@admin.register(Eval)
class EvalAdmin(admin.ModelAdmin):
    list_display = ('student', 'parcours', 'module', 'challenge', 'author', 'comment')
