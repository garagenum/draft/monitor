import datetime
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.shortcuts import render, get_object_or_404
from .models import Profile, Module, Skill, Challenge, Mission, Workshop, Session, Partner, Parcours, Eval
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth.decorators import permission_required, login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from .forms import CreateUserForm, CreateModuleForm, CreateSkillForm, CreateChallengeForm, CreateMissionForm, CreateWorkshopForm, CreateSessionForm, CreatePartnerForm, CreateParcoursForm, CreateEvalForm
from .forms import UpdateUserForm, UpdateModuleForm, UpdateSkillForm, UpdateChallengeForm, UpdateMissionForm, UpdateWorkshopForm, UpdateSessionForm, UpdatePartnerForm, UpdateParcoursForm, UpdateEvalForm
from django.forms.widgets import HiddenInput
from django.template import loader
from .utils import ModelUtils as MU
from django.core import serializers

# Create your views here.   

def index(request):
    """View function for home page of site."""
    num_users = User.objects.all().count()
    num_modules = Module.objects.all().count()
    num_workshops = Workshop.objects.all().count()
    num_evals = Eval.objects.all().count()
    num_challenges = Challenge.objects.all().count()
    num_missions = Mission.objects.all().count()
    num_skills = Skill.objects.all().count()
    num_partners = Partner.objects.all().count()

    # Number of visits to this view, as counted in the session variable.    
    num_visits = request.session.get('num_visits', 0)
    request.session['num_visits'] = num_visits + 1

    stats =  {
            'users': { 'num': num_users, 'objectif': 60, 'percent': round(num_users*100/60), 'name': "Participants", 'desc': "Des jeunes motivés" },
            'modules': { 'num': num_modules, 'objectif': 20, 'percent': round(num_modules*100/20), 'name': "Modules", 'desc': "De Python à Bash en passant par LesBonsClics" },
            'workshops': { 'num': num_workshops, 'objectif': 120, 'percent': round(num_workshops*100/120), 'name': "Ateliers collectifs", 'desc': "Des ateliers d'une demi-journée ou des activités sur une semaine"},
            'evals': {'num': num_evals, 'objectif': 60, 'percent': round(num_evals*100/60), 'name': "Évaluations", 'desc': "Réalisées par des pros" }, 
            'challenges': {'num': num_challenges, 'objectif': 60, 'percent': round(num_challenges*100/60), 'name': "Challenges", 'desc': "Des défis à relever pour progresser"},
            'missions': {'num': num_missions, 'objectif': 60, 'percent': round(num_missions*100/60), 'name': "Missions", 'desc': "Des réalisations concrètes au service des habitants"},
            'skills': {'num': num_skills, 'objectif': 60, 'percent': round(num_skills*100/60), 'name': "Compétences", 'desc': "Des compétences acquises ou à acquérir"},
            'partners': {'num': num_partners, 'objectif': 10, 'percent': round(num_partners*100/10), 'name': "Partenaires", 'desc': "Des organismes de formation, et des entreprises qui nous font confiance"},
    }
 
    context = {
        'stats': stats,
        'num_visits': {'num': num_visits},
    }
    html_template = loader.get_template('index.html')
    #return HttpResponse(html_template.render(context, request))
    return render(request, 'index.html', context=context)


###### Views for User

class UserListView(LoginRequiredMixin, generic.ListView):
    model = User
    paginate_by = 3
    template_name = 'sas/user_list.html'

class UserDetailView(PermissionRequiredMixin, generic.DetailView):
    permission_required = "can_evaluate_student"
    model = User
    template_name = 'sas/user_detail.html'

class UserCreate(PermissionRequiredMixin, CreateView):
    permission_required = "can_evaluate_student"
    model = User
    form_class = CreateUserForm
    #initial = {'date_of_death': '11/06/2020'}

class UserUpdate(UpdateView):
    model = User
    form_class = UpdateUserForm
    template_name = "sas/user_update.html"


class UserDelete(DeleteView):
    model = User
    success_url = reverse_lazy('authors')



####### Views for Module

class ModuleListView(generic.ListView):
    model = Module
    paginate_by = 3

class ModuleDetailView(LoginRequiredMixin, generic.DetailView):
    model = Module

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        o = context['object']
        context['is_detail_view'] = True
        context['fields'] = ['name', 'summary', 'description']
        context['related'] = []
        context['related'].append(MU.get_many_related(o, 'modules', Challenge, 'name', 'description'))
        context['related'].append(MU.get_many_related(o, 'modules', Skill, 'name', 'description'))
        context['related'].append(MU.get_many_related(o, 'modules', Workshop, 'name'))
        context['related'].append(MU.get_many_related(o, 'modules', Parcours, 'user', 'type_of_convention', 'tiers'))
        context['related'].append(MU.get_fk_related(o, 'module', Session, 'user', 'note', 'get_absolute_url'))
        return context

class ModuleCreate(PermissionRequiredMixin, CreateView):
    permission_required = "can_evaluate_student"
    model = Module
    form_class = CreateModuleForm

class ModuleUpdate(PermissionRequiredMixin, UpdateView):
    permission_required = "can_evaluate_student"
    model = Module
    form_class = UpdateModuleForm
    template_name = "sas/module_update.html"
    def get_context_data(self, **kwargs):
       context = super().get_context_data(**kwargs)
       context['edit'] = True
       context['hidden'] = 'name'
       print("inview : ")
       print(context)
       context['form'].fields['name'].widget = HiddenInput()
       return context


class ModuleDelete(PermissionRequiredMixin, DeleteView):
    permission_required = "can_evaluate_student"
    model = Module
    success_url = reverse_lazy('modules')


######## Views for Skill

class SkillListView(generic.ListView):
    model = Skill
    paginate_by = 3

class SkillDetailView(generic.DetailView):
    model = Skill
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        o = context['object']
        context['is_detail_view'] = True
        context['fields'] = MU.get_fields(o, 'name', 'description', 'modules', 'difficulty')
        context['related'] = []
        context['related'].append(MU.get_fk_related(o, 'main_skill', Challenge, 'name', 'description'))
        context['related'].append(MU.get_many_related(o, 'skills', Challenge, 'name', 'description'))

        return context

class SkillCreate(PermissionRequiredMixin, CreateView):
    permission_required = "can_evaluate_student"
    model = Skill
    form_class = CreateSkillForm
    def get_context_data(self, **kwargs):
       context = super().get_context_data(**kwargs)
       context["form"].fields['difficulty'].horizontal = True
       return context

class SkillUpdate(PermissionRequiredMixin, UpdateView):
    permission_required = "can_evaluate_student"
    model = Skill
    form_class = UpdateSkillForm
    template_name = "sas/skill_update.html"
    def get_context_data(self, **kwargs):
       context = super().get_context_data(**kwargs)
       print("kwargs")
       print(kwargs)
       print(context['view'].kwargs)
       print(context['view'].request.user)

       #context["form"].fields['modules'].queryset = Module.objects.filter(name="Django")
       return context


class SkillDelete(PermissionRequiredMixin, DeleteView):
    permission_required = "can_evaluate_student"
    model = Skill
    success_url = reverse_lazy('skills')


############ Views for Workshop

class WorkshopListView(generic.ListView):
    model = Workshop
    paginate_by = 3

class WorkshopDetailView(generic.DetailView):
    model = Workshop
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form"].fields['monitor'].queryset = User.objects.filter(profile_ismonitor=True)
        o = context['object']
        context['is_detail_view'] = True
        context['fields'] = MU.get_fields(o, 'name', 'description', 'start_date', 'end_date', 'difficulty', 'monitor', 'challenges', 'modules', 'skills')
        context['related'] = []
        context['related'].append(MU.get_fk_related(o, 'workshop', Eval, 'student', 'author', 'comment'))
        context['related'].append(MU.get_fk_related(o, 'workshop', Session, 'user', 'note', 'start_date', 'end_date'))

class WorkshopCreate(PermissionRequiredMixin, CreateView):
    permission_required = "can_evaluate_student"
    model = Workshop
    form_class = CreateWorkshopForm
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form"].fields['difficulty'].horizontal = True
        context["form"].fields['monitor'].queryset = User.objects.filter(profile__ismonitor=True)
        return context

class WorkshopUpdate(PermissionRequiredMixin, UpdateView):
    permission_required = "can_evaluate_student"
    model = Workshop
    form_class = UpdateWorkshopForm
    template_name = "sas/workshop_update.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form"].fields['difficulty'].horizontal = True
        context["form"].fields['monitor'].queryset = User.objects.filter(profile__ismonitor=True)
        return context

class WorkshopDelete(PermissionRequiredMixin, DeleteView):
    permission_required = "can_evaluate_student"
    model = Workshop
    success_url = reverse_lazy('workshops')


############# Views for Session

class SessionListView(generic.ListView):
    model = Session
    paginate_by = 3

class SessionDetailView(generic.DetailView):
    model = Session
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        o = context['object']
        context['is_detail_view'] = True
        context['related'] = []
       
class SessionCreate(LoginRequiredMixin, CreateView):
    model = Session
    form_class = CreateSessionForm

class SessionUpdate(LoginRequiredMixin, UpdateView):
    model = Session
    form_class = UpdateSessionForm
    template_name = "sas/session_update.html"


class SessionDelete(DeleteView):
    model = Session
    success_url = reverse_lazy('sessions')


############# Views for Challenge

class ChallengeListView(generic.ListView):
    model = Challenge
    paginate_by = 3

class ChallengeDetailView(generic.DetailView):
    model = Challenge
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        o = context['object']
        context['is_detail_view'] = True
        context['fields'] = MU.get_fields(o, 'name', 'description', 'difficulty', 'modules', 'challenges', 'skills')
        context['related'] = []
        context['related'].append(MU.get_fk_related(o, 'challenge', Eval, 'student', 'author', 'comment'))
        context['related'].append(MU.get_fk_related(o, 'challenge', Mission, 'participants', 'start_date', 'end_date'))
        context['related'].append(MU.get_many_related(o, 'challenges', Workshop, 'name', 'description'))
        context['related'].append(MU.get_many_related(o, 'challenges', Challenge, 'name', 'description'))


class ChallengeCreate(PermissionRequiredMixin, CreateView):
    permission_required = "can_evaluate_student"
    model = Challenge
    form_class = CreateChallengeForm
    #fields = ['name', 'description', 'start_date', 'end_date', 'participants', 'challenges', 'modules', 'skills', 'monitor']
    def get_context_data(self, **kwargs):
       context = super().get_context_data(**kwargs)
       context["form"].fields['difficulty'].horizontal = True
       return context

class ChallengeUpdate(PermissionRequiredMixin, UpdateView):
    permission_required = "can_evaluate_student"
    model = Challenge
    form_class = UpdateChallengeForm
    template_name = "sas/challenge_update.html"


class ChallengeDelete(PermissionRequiredMixin, DeleteView):
    permission_required = "can_evaluate_student"
    model = Challenge
    success_url = reverse_lazy('challenges')


############# Views for Mission

class MissionListView(generic.ListView):
    model = Mission
    paginate_by = 3

class MissionDetailView(generic.DetailView):
    model = Mission
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        o = context['object']       
        context['is_detail_view'] = True
        context['fields'] = MU.get_fields(o, 'challenge', 'context', 'participants', 'start_date', 'end_date', 'monitor')
        context['related'] = []
        context['related'].append(MU.get_many_related(o, 'missions', Parcours, 'user', 'type_of_convention', 'tiers'))
        context['related'].append(MU.get_fk_related(o, 'mission', Session, 'user', 'note', 'start_date', 'end_date'))
        return context
       
class MissionCreate(PermissionRequiredMixin, CreateView):
    permission_required = "can_evaluate_student"
    model = Mission
    form_class = CreateMissionForm
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form"].fields['monitor'].queryset = User.objects.filter(profile__ismonitor=True)
        P = Parcours.objects.filter(
            end_date__date__gte=datetime.datetime.now()).order_by().values('user').distinct()
        context["form"].fields['participants'].queryset = User.objects.filter(id__in=P, profile__ismonitor=False)
        return context

class MissionUpdate(PermissionRequiredMixin, UpdateView):
    permission_required = "can_evaluate_student"
    model = Mission
    form_class = UpdateMissionForm
    template_name = "sas/mission_update.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form"].fields['monitor'].queryset = User.objects.filter(profile__ismonitor=True)
        P = Parcours.objects.filter(
            end_date__date__gte=context['object'].end_date, 
            start_date__date__lte=context['object'].start_date).order_by().values('user').distinct()
        context["form"].fields['participants'].queryset = User.objects.filter(id__in=P, profile__ismonitor=False)
        return context


class MissionDelete(PermissionRequiredMixin, DeleteView):
    permission_required = "can_evaluate_student"
    model = Mission
    success_url = reverse_lazy('missions')

 
############ Views for Partner

class PartnerListView(generic.ListView):
    model = Partner
    paginate_by = 3

class PartnerDetailView(generic.DetailView):
    model = Partner
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        o = context['object']
        context['is_detail_view'] = True
        context['fields'] = MU.get_fields(o, 'name', 'address', 'intermediary')
        context['related'] = []
        context['related'].append(MU.get_fk_related(o, 'tiers', Parcours, 'user', 'type_of_convention', 'tiers'))
        context['related'].append(MU.get_many_related(o, 'other_partners', Parcours, 'user', 'type_of_convention', 'tiers'))
        return context
       
class PartnerCreate(PermissionRequiredMixin, CreateView):
    permission_required = "can_evaluate_student"
    model = Partner
    form_class = CreatePartnerForm
    #fields = ['name', 'address', 'intermediary']
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form"].fields['intermediary'].queryset = User.objects.filter(profile__ismonitor=True)
        return context

class PartnerUpdate(PermissionRequiredMixin, UpdateView):
    permission_required = "can_evaluate_student"
    model = Partner
    form_class = UpdatePartnerForm
    template_name = "sas/partner_update.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form"].fields['intermediary'].queryset = User.objects.filter(profile__ismonitor=True)
        return context


class PartnerDelete(PermissionRequiredMixin, DeleteView):
    permission_required = "can_evaluate_student"
    model = Partner
    success_url = reverse_lazy('partners')


############ Views for Parcours 

class ParcoursListView(generic.ListView):
    model = Parcours
    paginate_by = 3

class ParcoursDetailView(generic.DetailView):
    model = Parcours
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        o = context['object']
        context['is_detail_view'] = True
        context['fields'] = MU.get_fields(o, 'user', 'tiers', 'monitor', 'type_of_convention', 'other_partners', 'start_date', 'end_date', 'modules', 'challenges')
        context['related'] = []
        context['related'].append(MU.get_fk_related(o, 'parcours', Eval, 'student', 'author', 'comment'))
        context['related'].append(MU.get_fk_related(o, 'parcours', Session, 'user', 'note', 'start_date', 'end_date'))
        return context
       
class ParcoursCreate(PermissionRequiredMixin, CreateView):
    permission_required = "can_evaluate_student"
    model = Parcours
    form_class = CreateParcoursForm
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form"].fields['user'].queryset = User.objects.filter(profile__ismonitor=False)
        context["form"].fields['monitor'].queryset = User.objects.filter(profile__ismonitor=True)
        context["challenges_by_module"] = {}
        for mod in Module.objects.all():
            context["challenges_by_module"][mod.id] = [chal.id for chal in mod.challenge_modules.all()]
        context["tiers_jsondata"] = serializers.serialize('json', Partner.objects.all())
        #context["form"].jdata['fields'] = serializers.serialize('json', Partner.objects.all())
        #context["hidden"] = 'other_partners'
        #context["form"].fields['other_partners'].widget = HiddenInput()
        return context


class ParcoursUpdate(PermissionRequiredMixin, UpdateView):
    permission_required = "can_evaluate_student"
    model = Parcours
    form_class = UpdateParcoursForm
    template_name = "sas/parcours_update.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        print("context in update parcours view : ")
        context["form"].fields['user'].queryset = User.objects.filter(profile__ismonitor=False)
        context["form"].fields['monitor'].queryset = User.objects.filter(profile__ismonitor=True)
        #context["form"].fields['other_partners'].queryset = Partner.objects.exclude(id=context['object'].tiers.id)
        context["challenges_by_module"] = {}
        for mod in Module.objects.all():
            context["challenges_by_module"][mod.id] = [chal.id for chal in mod.challenge_modules.all()]
        context["tiers_jsondata"] = serializers.serialize('json', Partner.objects.all())
        print(context)
        print("=========---------============")
        return context


class ParcoursDelete(PermissionRequiredMixin, DeleteView):
    permission_required = "can_evaluate_student"
    model = Parcours
    success_url = reverse_lazy('parcourss')


######### Views for Eval

class EvalDetailView(PermissionRequiredMixin, generic.DetailView):
    permission_required = "can_evaluate_student"
    model = Eval
    template_name = 'sas/eval_detail.html' 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        o = context['object']
        context['is_detail_view'] = True
        context['fields'] = MU.get_fields(o, 'student', 'parcours', 'module', 'challenge', 'workshop', 'comment', 'author')
        context['related'] = []
        return context
       
class EvalListView(PermissionRequiredMixin, generic.ListView):
    permission_required = "can_evaluate_student"
    model = Eval
    paginate_by = 3
    template_name = "sas/eval_list.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context
    
class EvalCreate(PermissionRequiredMixin, CreateView):
    permission_required = "can_evaluate_student"
    model = Eval
    form_class = CreateEvalForm
    #fields = ['user', 'tiers', 'monitor', 'type_of_convention', 'other_partners', 'start_date', 'end_date', 'modules', 'challenges']
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form"].fields['student'].queryset = User.objects.filter(profile__ismonitor=False)
        context["form"].fields['author'].queryset = User.objects.filter(profile__ismonitor=True)
        return context

class EvalUpdate(PermissionRequiredMixin, UpdateView):
    permission_required = "can_evaluate_student"
    model = Eval
    form_class = UpdateEvalForm
    template_name = "sas/eval_update.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form"].fields['student'].queryset = User.objects.filter(profile__ismonitor=False)
        context["form"].fields['author'].queryset = User.objects.filter(profile__ismonitor=True)
        return context

class EvalDelete(PermissionRequiredMixin, DeleteView):
    permission_required = "can_evaluate_student"
    model = Eval
    success_url = reverse_lazy('evals')