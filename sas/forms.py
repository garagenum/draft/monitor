import datetime
from django import forms
from django.forms import ModelForm, Form
from .models import Profile, Module, Skill, Challenge, Mission, Workshop, Session, Partner, Parcours, Eval
from django.contrib.auth.models import User
from django.utils.translation import gettext as _

from django.core.exceptions import ValidationError
from pprint import pprint
import logging
logger = logging.getLogger(__name__)

class CreateModuleForm(ModelForm):
    class Meta:
        model = Module
        fields = ['name', 'summary', 'description']
        widgets = {
            'name': forms.TextInput(attrs={'class': "form-control", 'placeholder': "bbbbbbref"}),
            'summary': forms.Textarea(attrs={'class': "form-control"}),
            'description': forms.Textarea(attrs={'class': "form-control"}),
        }

class UpdateModuleForm(ModelForm):

    class Meta:
        model = Module
        fields = ['name', 'summary', 'description']
        widgets = {
            'name': forms.TextInput(attrs={'class': "form-control", 'placeholder': "bbbbbbref"}),
            'summary': forms.Textarea(attrs={'class': "form-control"}),
            'description': forms.Textarea(attrs={'class': "form-control"}),
        }

class CreateSkillForm(ModelForm):
    class Meta:
        model = Skill
        fields = ['name', 'description', 'modules', 'difficulty']
        help_texts = {
            'modules': "Sélectionnez les modules qui sont susceptibles d'utiliser cette compétence",
            'difficulty': "1 = facile, 5 = expert"
        }
        widgets = {
            'name': forms.TextInput(attrs={'class': "form-control", 'placeholder': "bbbbbbref"}),
            'description': forms.Textarea(attrs={'class': "form-control"}),
            'modules': forms.SelectMultiple(attrs={'id': "selectbox_skill_modules"}),
            'difficulty': forms.RadioSelect(attrs={'class': "form-check-input"}),
        }
class UpdateSkillForm(ModelForm):

    class Meta:
        model = Skill
        fields = ['name', 'description', 'modules', 'difficulty']
        help_texts = {
            'modules': "Sélectionnez les modules qui sont susceptibles d'utiliser cette compétence",
            'difficulty': "1 = facile, 5 = expert"
        }
        widgets = {
            'name': forms.TextInput(attrs={'class': "form-control", 'placeholder': "bbbbbbref"}),
            'description': forms.Textarea(attrs={'class': "form-control"}),
            'modules': forms.SelectMultiple(attrs={'id': "selectbox_skill_modules"}),
            'difficulty': forms.RadioSelect(attrs={'class': "form-check-input"}),
        }



class CreateChallengeForm(ModelForm):
    class Meta:
        model = Challenge
        fields = ['name', 'description', 'main_skill', 'skills', 'modules', 'difficulty']
        help_texts = {
            'main_skill': "La compétence centrale acquise avec ce challenge",
            'skills': "Les autres compétences travaillées avec ce challenge",
            'modules': "Les modules concernés par ce challenge",
            'difficulty': "1 = facile, 5 = expert"

        }
        widgets = {
            'name': forms.TextInput(attrs={'class': "form-control", 'placeholder': "Écrivez ici"}),
            'description': forms.Textarea(attrs={'class': "form-control"}),
            'main_skill': forms.Select(attrs={'id': "selectbox_challenge_main-skill"}),
            'skills': forms.SelectMultiple(attrs={'id': "selectbox_challenge_skills"}),            
            'modules': forms.SelectMultiple(attrs={'id': "selectbox_challenge_modules"}),          
            'difficulty': forms.RadioSelect(attrs={'class': "form-check-input"}),
        }

class UpdateChallengeForm(ModelForm):
    class Meta:
        model = Challenge
        fields = ['name', 'description', 'main_skill', 'skills', 'modules', 'difficulty']
        help_texts = {
            'main_skill': "La compétence centrale acquise avec ce challenge",
            'skills': "Les autres compétences travaillées avec ce challenge",
            'modules': "Les modules concernés par ce challenge",
            'difficulty': "1 = facile, 5 = expert"

        }
        widgets = {
            'name': forms.TextInput(attrs={'class': "form-control", 'placeholder': "Écrivez ici"}),
            'description': forms.Textarea(attrs={'class': "form-control"}),
            'main_skill': forms.Select(attrs={'id': "selectbox_challenge_main-skill"}),
            'skills': forms.SelectMultiple(attrs={'id': "selectbox_challenge_skills"}),            
            'modules': forms.SelectMultiple(attrs={'id': "selectbox_challenge_modules"}),            
            'difficulty': forms.RadioSelect(attrs={'class': "form-check-input"}),
        }

class CreateMissionForm(ModelForm):
    start_date = forms.SplitDateTimeField()
    end_date = forms.SplitDateTimeField()
    
    def clean(self):
        data = super().clean()
        start_date = data.get('start_date')
        end_date = data.get('end_date')
        participants = data.get('participants')
        P = Parcours.objects.filter(
            start_date__date__lte=start_date, 
            end_date__date__gte=end_date).values_list('user', flat=True).order_by().distinct()
        participants_cant = []
        for i in participants:
            if i.id not in P:
                participants_cant.append(i.profile.__str__())
        if participants_cant:
            self.add_error('participants', forms.ValidationError(
                _('Les étudiants suivants n\'ont pas de parcours sur les dates de cette mission : %(students)s'),
                params={'students': ','.join(map(str, participants_cant))}
                )
            )

    class Meta:
        model = Mission
        fields = ['challenge', 'start_date', 'end_date', 'monitor', 'context', 'participants']
        help_texts = {
            'challenge': "Une mission permet de réaliser un challenge-type",
            'monitor': "Encadrant ou tuteur du groupe",
            'context': "Apportez des précisions quant à la réalisation de l'action",
            'participants': "Les étudiants qui concourent à la réalisation de l'action",
        }
        widgets = {
            'challenge': forms.Select(attrs={'id': "selectbox_mission_challenge"}),
            'monitor': forms.Select(attrs={'id': "selectbox_mission_monitor"}),
            'context': forms.Textarea(attrs={'class': "form-control"}),
            'participants': forms.SelectMultiple(attrs={'id': "selectbox_mission_participants"}),
            'start_date': forms.SplitDateTimeWidget(attrs={}),
            'end_date': forms.SplitDateTimeWidget(),
        }

class UpdateMissionForm(ModelForm):
    start_date = forms.SplitDateTimeField()
    end_date = forms.SplitDateTimeField()
    
    def clean(self):
        data = super().clean()
        start_date = data.get('start_date')
        end_date = data.get('end_date')
        participants = data.get('participants')
        P = Parcours.objects.filter(
            start_date__date__lte=start_date, 
            end_date__date__gte=end_date).values_list('user', flat=True).order_by().distinct()
        participants_cant = []
        for i in participants:
            if i.id not in P:
                participants_cant.append(i.profile.__str__())
        if participants_cant:
            self.add_error('participants', forms.ValidationError(
                _('Les étudiants suivants n\'ont pas de parcours sur les dates de cette mission : %(students)s'),
                params={'students': ','.join(map(str, participants_cant))}
                )
            )


    class Meta:
        model = Mission
        fields = ['challenge', 'start_date', 'end_date', 'monitor', 'context', 'participants']
        help_texts = {
            'challenge': "Une mission permet de réaliser un challenge-type",
            'monitor': "Encadrant ou tuteur du groupe",
            'context': "Apportez des précisions quant à la réalisation de l'action",
            'participants': "Les étudiants qui concourent à la réalisation de l'action",
        }
        widgets = {
            'challenge': forms.Select(attrs={'id': "selectbox_mission_challenge"}),
            'monitor': forms.Select(attrs={'id': "selectbox_mission_monitor"}),
            'context': forms.Textarea(attrs={'class': "form-control"}),
            'participants': forms.SelectMultiple(attrs={'id': "selectbox_mission_participants"}),
            'start_date': forms.SplitDateTimeWidget(attrs={}),
            'end_date': forms.SplitDateTimeWidget(),
        }

class CreateWorkshopForm(ModelForm):
    start_date = forms.SplitDateTimeField()
    end_date = forms.SplitDateTimeField()
    class Meta:
        model = Workshop
        fields = ["name", "description", "start_date", "end_date", "difficulty", "monitor", "challenges", "modules", "skills"]
        help_texts = {
            'challenges': "Si l'atelier est consacré à un ou des challenges spécifiques",
            'monitor': "Encadrant ou tuteur du groupe",
            'modules': "Les modules sur lesquels l'atelier permet d'avancer",
            'skills': "Les compétences que l'atelier permet d'acquérir",
            'difficulty': "1 = facile, 5 = expert",
        }
        widgets = {
            'name': forms.TextInput(attrs={'class': "form-control", 'placeholder': "Écrivez ici"}),
            'description': forms.Textarea(attrs={'class': "form-control"}),
            'start_date': forms.SplitDateTimeWidget(attrs={}),
            'end_date': forms.SplitDateTimeWidget(),
            'monitor': forms.Select(attrs={'id': "selectbox_workshop_monitor"}),
            'modules': forms.SelectMultiple(attrs={'id': "selectbox_workshop_modules"}),            
            'challenges': forms.SelectMultiple(attrs={'id': "selectbox_workshop_challenges"}),            
            'skills': forms.SelectMultiple(attrs={'id': "selectbox_workshop_skills"}),
            'difficulty': forms.RadioSelect(attrs={'class': "form-check-input"}),
        }

class UpdateWorkshopForm(ModelForm):
    start_date = forms.SplitDateTimeField()
    end_date = forms.SplitDateTimeField()
    class Meta:
        model = Workshop
        fields = ["name", "description", "start_date", "end_date", "difficulty", "monitor", "challenges", "modules", "skills"]
        help_texts = {
            'challenges': "Si l'atelier est consacré à un ou des challenges spécifiques",
            'monitor': "Encadrant ou tuteur du groupe",
            'modules': "Les modules sur lesquels l'atelier permet d'avancer",
            'skills': "Les compétences que l'atelier permet d'acquérir",
            'difficulty': "1 = facile, 5 = expert",
        }
        widgets = {
            'name': forms.TextInput(attrs={'class': "form-control", 'placeholder': "Écrivez ici"}),
            'description': forms.Textarea(attrs={'class': "form-control"}),
            'start_date': forms.SplitDateTimeWidget(attrs={}),
            'end_date': forms.SplitDateTimeWidget(),
            'monitor': forms.Select(attrs={'id': "selectbox_workshop_monitor"}),
            'modules': forms.SelectMultiple(attrs={'id': "selectbox_workshop_modules"}),            
            'challenges': forms.SelectMultiple(attrs={'id': "selectbox_workshop_challenges"}),            
            'skills': forms.SelectMultiple(attrs={'id': "selectbox_workshop_skills"}),
            'difficulty': forms.RadioSelect(attrs={'class': "form-check-input"}),
        }

class CreateSessionForm(ModelForm):
    start_date = forms.SplitDateTimeField()
    end_date = forms.SplitDateTimeField()
    class Meta:
        model = Session
        fields = ["user", "start_date", "end_date", "module", "mission", "workshop", "note"]
        help_texts = {
            'user': "l'utilisateur en cours",
            'module': "Le module travaillé pendant cette session",
            'mission': "La mission sur laquelle j'ai avancé pendant cette session",
            'workshop': "L'atelier auquel j'ai participé pendant cette session",
            'note': "Écrivez ici vos notes de session",
        }
        widgets = {
            'user': forms.Select(attrs={'id': "selectbox_session_user"}),
            'start_date': forms.SplitDateTimeWidget(attrs={}),
            'end_date': forms.SplitDateTimeWidget(),
            'module': forms.Select(attrs={'id': "selectbox_session_module"}),
            'mission': forms.Select(attrs={'id': "selectbox_session_mission"}),
            'workshop': forms.Select(attrs={'id': "selectbox_session_workshop"}),
            'note': forms.Textarea(attrs={'class': "form-control"}),
        }

class UpdateSessionForm(ModelForm):
    start_date = forms.SplitDateTimeField()
    end_date = forms.SplitDateTimeField()
    class Meta:
        model = Session
        fields = ["user", "start_date", "end_date", "module", "mission", "workshop", "note"]
        help_texts = {
            'user': "l'utilisateur en cours",
            'module': "Le module travaillé pendant cette session",
            'mission': "La mission sur laquelle j'ai avancé pendant cette session",
            'workshop': "L'atelier auquel j'ai participé pendant cette session",
            'note': "Écrivez ici vos notes de session",
        }
        widgets = {
            'user': forms.Select(attrs={'id': "selectbox_session_user"}),
            'start_date': forms.SplitDateTimeWidget(attrs={}),
            'end_date': forms.SplitDateTimeWidget(),
            'module': forms.Select(attrs={'id': "selectbox_session_module"}),
            'mission': forms.Select(attrs={'id': "selectbox_session_mission"}),
            'workshop': forms.Select(attrs={'id': "selectbox_session_workshop"}),
            'note': forms.Textarea(attrs={'class': "form-control"}),
        }

class CreatePartnerForm(ModelForm):
    class Meta:
        model = Partner
        fields = ['name', 'address', 'intermediary']
        help_texts: {
            'name': "Le nom de la structure",
            'address': "Adresse de la structure",
            'intermediary': "La personne de la structure partenaire en charge des relations avec le Garage Numérique",
        }
        widgets = {
            'name': forms.TextInput(attrs={'class': "form-control",  'placeholder': "bbbbbbref"}),
            'address':  forms.TextInput(attrs={'class': "form-control",  'placeholder': "bbbbbbref"}),
            'intermediary': forms.Select(attrs={'class': "form-select tomselected", 'id': 'selectbox', 'tabindex': "-1", 'hidden': "hidden"}),
        }

class UpdatePartnerForm(ModelForm):
    class Meta:
        model = Partner
        fields = ['name', 'address', 'intermediary']
        help_texts: {
            'name': "Le nom de la structure",
            'address': "Adresse de la structure",
            'intermediary': "La personne de la structure partenaire en charge des relations avec le Garage Numérique",
        }
        widgets = {
            'name': forms.TextInput(attrs={'class': "form-control",  'placeholder': "bbbbbbref"}),
            'address':  forms.TextInput(attrs={'class': "form-control",  'placeholder': "bbbbbbref"}),
            'intermediary': forms.Select(attrs={'class': "form-select tomselected", 'id': 'selectbox', 'tabindex': "-1", 'hidden': "hidden"}),
        }

class CreateParcoursForm(ModelForm):
    start_date = forms.SplitDateTimeField()
    end_date = forms.SplitDateTimeField()
    class Meta:
        model = Parcours
        fields = ["user", "start_date", "end_date", "monitor", "type_of_convention", "tiers", "other_partners", "modules", "challenges"]
        help_texts = {
            'user': "L'utilisateur en cours",
            'tiers': "L'organisme externe avec qui la convention est passée",
            'monitor': "Le formateur en charge du suivi du parcours",
            'type_of_convention': "La convention signée entre l'étudiant, le Garage Numérique et un organisme tiers",
            'other_partners': "Si d'autres  partenaires contribuent au programme",
            'modules': "Les modules à travailler pendant le parcours",
            'challenges': "Les challenges à relever pendant le parcours",
        }
        widgets = {
            'user': forms.Select(attrs={'id': "selectbox_parcours_user"}),
            'start_date': forms.SplitDateTimeWidget(attrs={}),
            'end_date': forms.SplitDateTimeWidget(),
            'monitor': forms.Select(attrs={'id': "selectbox_parcours_monitor"}),
            'type_of_convention': forms.Select(attrs={'id': "selectbox_parcours_convention"}),
            'tiers': forms.Select(attrs={'id': "selectbox_parcours_tiers"}),
            'other_partners': forms.SelectMultiple(attrs={'id': "selectbox_parcours_partners"}),
            'modules': forms.SelectMultiple(attrs={'id': "selectbox_parcours_modules"}),            
            'challenges': forms.SelectMultiple(attrs={'id': "selectbox_parcours_challenges"}),    
        }

class UpdateParcoursForm(ModelForm):
    start_date = forms.SplitDateTimeField()
    end_date = forms.SplitDateTimeField()
    class Meta:
        model = Parcours
        fields = ["user", "start_date", "end_date", "monitor", "tiers", "type_of_convention", "tiers", "other_partners", "modules", "challenges"]
        help_texts = {
            'user': "L'utilisateur en cours",
            'tiers': "L'organisme externe avec qui la convention est passée",
            'monitor': "Le formateur en charge du suivi du parcours",
            'type_of_convention': "La convention signée entre l'étudiant, le Garage Numérique et un organisme tiers",
            'other_partners': "Si d'autres  partenaires contribuent au programme",
            'modules': "Les modules à travailler pendant le parcours",
            'challenges': "Les challenges à relever pendant le parcours",
        }
        widgets = {
            'user': forms.Select(attrs={'id': "selectbox_parcours_user"}),
            'start_date': forms.SplitDateTimeWidget(attrs={}),
            'end_date': forms.SplitDateTimeWidget(),
            'monitor': forms.Select(attrs={'id': "selectbox_parcours_monitor"}),
            'type_of_convention': forms.Select(attrs={'id': "selectbox_parcours_convention"}),
            'tiers': forms.Select(attrs={'id': "selectbox_parcours_tiers"}),
            'other_partners': forms.SelectMultiple(attrs={'id': "selectbox_parcours_partners"}),
            'modules': forms.SelectMultiple(attrs={'id': "selectbox_parcours_modules"}),            
            'challenges': forms.SelectMultiple(attrs={'id': "selectbox_parcours_challenges"}),    
        }


class CreateEvalForm(ModelForm):
    class Meta:
        model = Eval
        fields = ["student", "parcours", "module", "challenge", "workshop", "comment", "author"]
        help_texts = {
            'student': "l'étudiant a évaluer",
            'module': "Le module à évaluer",
            'challenge': "Le challenge à évaluer",
            'workshop': "L'atelier à évaluer",
            'comment': "Votre évaluation",
            'author': "L'évaluateur",
        }
        widgets = {
            'student': forms.Select(attrs={'id': "selectbox_eval_student"}),
            'parcours': forms.Select(),
            'module': forms.Select(),
            'challenge': forms.Select(attrs={'id': "selectbox_eval_challenge"}),
            'workshop': forms.Select(attrs={'id': "selectbox_eval_workshop"}),
            'comment': forms.Textarea(attrs={'class': "form-control"}),
            'author': forms.Select(attrs={'id': "selectbox_eval_author"}),
        }

class UpdateEvalForm(ModelForm):
    class Meta:
        model = Eval
        fields = ["student", "parcours", "module", "challenge", "workshop", "comment", "author"]
        help_texts = {
            'student': "l'étudiant a évaluer",
            'module': "Le module à évaluer",
            'challenge': "Le challenge à évaluer",
            'workshop': "L'atelier à évaluer",
            'comment': "Votre évaluation",
            'author': "L'évaluateur",
        }
        widgets = {
            'student': forms.Select(attrs={'id': "selectbox_eval_student"}),
            'parcours': forms.Select(),
            'module': forms.Select(),
            'challenge': forms.Select(attrs={'id': "selectbox_eval_challenge"}),
            'workshop': forms.Select(attrs={'id': "selectbox_eval_workshop"}),
            'comment': forms.Textarea(attrs={'class': "form-control"}),
            'author': forms.Select(attrs={'id': "selectbox_eval_author"}),
        }

class CreateUserForm(ModelForm):
    pass
class UpdateUserForm(ModelForm):
    pass


"""
class CreateSessionForm(ModelForm):
    class Meta:
        model = Session
        fields = '__all__'
    def __init__(self, *args, **kwargs):
        pk_module = kwargs.pop('pk_module', None)
        super(ModelForm, self).__init__(*args, **kwargs)
        if pk_module !=0:
            self.fields["module"].widget = forms.HiddenInput()
            self.fields["module"].initial = Module.objects.get(id=pk_module)

    def clean(self):
        super().clean()
        data = self.cleaned_data
        if data['end_date'] < data['start_date']:
            raise forms.ValidationError({'end_date': u'Error: The ending date must come after the starting date.'})
        self.instance.save()
        for participant in data['participants'].all():
            participant.profile.sessions.add(self.instance)
            participant.profile.save()
            participant.save()
        return data

class UpdateSessionForm(ModelForm):
    class Meta:
        model = Session
        fields = '__all__'
    def __init__(self, *args, **kwargs):
        pk_module = kwargs.pop('pk_module', None)
        super(ModelForm, self).__init__(*args, **kwargs)
        self.fields["module"].widget = forms.HiddenInput()
        self.fields["module"].initial = Module.objects.get(id=pk_module)

    def clean(self):
        super().clean()
        data = self.cleaned_data
        if data['end_date'] < data['start_date']:
            raise forms.ValidationError({'end_date': u'Error: The ending date must come after the starting date.'})
        self.instance.save()
        for participant in data['participants'].all():
            participant.profile.sessions.add(self.instance)
            participant.profile.save()
            participant.save()
        return data

class CreateEvalForm(ModelForm):
    class Meta:
        model = Eval
        fields = ['user', 'comment', 'author']
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        pk_user = kwargs.pop('pk_user', None)
        pk_session = kwargs.pop('pk_session', None)
        super(ModelForm, self).__init__(*args, **kwargs)
        self.fields["moduleinstance"].queryset = Session.objects.filter(participants__id=pk_user)
        self.fields["user"].queryset = User.objects.filter(profile__sessions__id=pk_session)
        self.fields["author"].initial = user
        self.fields["author"].widget = forms.HiddenInput()
        self.fields["comment"].initial = "comment"

        if pk_user != 0:
            self.fields["user"].initial = User.objects.get(pk=pk_user)
            self.fields["user"].widget = forms.HiddenInput()
        for i in self.fields:
            print(self.fields[i].initial)

        if pk_session != 0:
            self.fields["session"].initial = Session.objects.get(pk=pk_session)
            self.fields["session"].widget = forms.HiddenInput()
        for i in self.fields:
            print(self.fields[i].initial)


    def clean(self, **kwargs):
        super().clean()
        data = self.cleaned_data
        user = data['user']
        session = data['session']
        self.instance.save()
        user.profile.evals.add(self.instance)
        user.profile.save()
        user.save()
        return data

class UpdateEvalForm(ModelForm):
    class Meta:
        model = Eval
        fields = ['user', 'comment', 'author']
    def __init__(self, *args, **kwargs):
        myuser = kwargs.pop('user', None)
        pk_user = kwargs.pop('pk_user', None)
        pk_session = kwargs.pop('pk_session', None)
        super(ModelForm, self).__init__(*args, **kwargs)
        self.fields["session"].queryset = Session.objects.filter(participants__id=pk_user)
        self.fields["author"].initial = myuser
        self.fields["author"].widget = forms.HiddenInput()
        #self.fields["comment"].initial = "comment"
        self.fields["user"].initial = User.objects.get(pk=pk_user)
        self.fields["user"].widget = forms.HiddenInput()
        if pk_session != 0:
            self.fields["session"].initial = Session.objects.get(pk=pk_session)
            self.fields["session"].widget = forms.HiddenInput()


    def clean(self, **kwargs):
        super().clean()
        data = self.cleaned_data
        user = data['user']
        session = data['session']
        self.instance.save()
        user.profile.evals.add(self.instance)
        user.profile.save()
        user.save()
        return data
"""