document.querySelectorAll('[id^="selectbox"]').forEach(item => {
    new TomSelect(item, { allowEmptyOption: true});
})

//const tiers_data = JSON.parse(document.getElementById('tiers_data').textContent);

var settings = {
    allowEmptyOption: true, 
};

const tiers_select = document.getElementById('selectbox_parcours_tiers').tomselect;
const other_partners_select = document.getElementById('selectbox_parcours_partners').tomselect;

other_partners_select.removeOption(tiers_select.getValue());

tiers_select.on('change', () => {
    other_partners_select.sync();
    other_partners_select.removeOption(tiers_select.getValue());
});

const parcours_modules_challenges = JSON.parse(document.getElementById('parcours_modules_challenges').textContent);
const parcours_modules_select = document.getElementById('selectbox_parcours_modules').tomselect;
const parcours_challenges_select = document.getElementById('selectbox_parcours_challenges').tomselect;

// Select available challenges on initialization
function select_available_challenges() {
    let modules_challenges_list = [];
    parcours_modules_select.getValue().forEach((moduleId, i) => {
        parcours_modules_challenges[moduleId].forEach((challengeId, i) => {
            modules_challenges_list.push(challengeId);
        });
    });
    let unique_modules_challenges_list = [...new Set(modules_challenges_list)];
    let modules_challenges_to_delete = [];
    let opts_as_array = Object.keys(parcours_challenges_select.options);
    const challenges_to_delete = Object.keys(parcours_challenges_select.options).filter(chal => !unique_modules_challenges_list.includes(Number(chal)));
    challenges_to_delete.forEach((chal, i) => {
        parcours_challenges_select.removeOption(Number(chal));
    });
}

select_available_challenges();

parcours_modules_select.on('change', () => { 
    parcours_challenges_select.sync();
    select_available_challenges();
});