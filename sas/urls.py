from django.urls import path, re_path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
]


urlpatterns += [
    path('users/', views.UserListView.as_view(), name='users'),
    path('user/<int:pk>/', views.UserDetailView.as_view(), name='user-detail'),
    path('user/<int:pk>/sessions/', views.SessionListView.as_view(), name='user-sessions'),
    path('monitor/<int:pk>/challenges', views.ChallengeListView.as_view(), name='monitor-challenges'),
    path('monitor/<int:pk>/parcourss', views.ParcoursListView.as_view(), name='monitor-parcours'),
    path('student/<int:pk>/challenges', views.ChallengeListView.as_view(), name='student-challenges'),
    path('student/<int:pk>/parcourss', views.ParcoursListView.as_view(), name='student-parcours'),
    path('user/create/', views.UserCreate.as_view(), name='user-create'),
    path('user/<int:pk>/update/', views.UserUpdate.as_view(), name='user-update'),
    path('user/<int:pk>/delete/', views.UserDelete.as_view(), name='user-delete'),
]


urlpatterns += [
    path('modules/', views.ModuleListView.as_view(), name='modules'),
    path('module/<int:pk>/', views.ModuleDetailView.as_view(), name='module-detail'),
    path('module/create/', views.ModuleCreate.as_view(), name='module-create'),
    path('module/<int:pk>/update/', views.ModuleUpdate.as_view(), name='module-update'),
    path('module/<int:pk>/delete/', views.ModuleDelete.as_view(), name='module-delete'),
]

urlpatterns += [
    path('skills/', views.SkillListView.as_view(), name='skills'),
    path('skill/<int:pk>/workshops/', views.WorkshopListView.as_view(), name='skill-workshops'),
    path('skill/<int:pk>/challenges', views.ChallengeListView.as_view(), name='skill-challenges'),
    path('skill/<int:pk>/', views.SkillDetailView.as_view(), name='skill-detail'),
    path('skill/create/', views.SkillCreate.as_view(), name='skill-create'),
    path('skill/<int:pk>/update/', views.SkillUpdate.as_view(), name='skill-update'),
    path('skill/<int:pk>/delete/', views.SkillDelete.as_view(), name='skill-delete'),
]

urlpatterns += [
    path('workshops/', views.WorkshopListView.as_view(), name='workshops'),
    path('workshop/<int:pk>/', views.WorkshopDetailView.as_view(), name='workshop-detail'),
    path('workshop/<int:pk>/sessions/', views.SessionListView.as_view(), name='workshop-sessions'),
    path('workshop/create/', views.WorkshopCreate.as_view(), name='workshop-create'),
    path('workshop/<int:pk>/update/', views.WorkshopUpdate.as_view(), name='workshop-update'),
    path('workshop/<int:pk>/delete/', views.WorkshopDelete.as_view(), name='workshop-delete'),
]

urlpatterns += [
    path('sessions/', views.SessionListView.as_view(), name='sessions'),
    path('session/<int:pk>/', views.SessionDetailView.as_view(), name='session-detail'),
    path('session/create/', views.SessionCreate.as_view(), name='session-create'),
    path('session/<int:pk>/update/', views.SessionUpdate.as_view(), name='session-update'),
    path('session/<int:pk>/delete/', views.SessionDelete.as_view(), name='session-delete'),
]

urlpatterns += [
    path('challenge/create/', views.ChallengeCreate.as_view(), name='challenge-create'),
    path('challenge/<int:pk>/', views.ChallengeDetailView.as_view(), name='challenge-detail'),
    path('challenge/<int:pk>/update/', views.ChallengeUpdate.as_view(), name='challenge-update'),
    path('challenge/<int:pk>/delete/', views.ChallengeDelete.as_view(), name='challenge-delete'),
    path('challenge/<int:pk>/sessions/', views.SessionListView.as_view(), name='challenge-sessions'),
    path('challenges/', views.ChallengeListView.as_view(), name='challenges'),
]

urlpatterns += [
    path('mission/create/', views.MissionCreate.as_view(), name='mission-create'),
    path('mission/<int:pk>/', views.MissionDetailView.as_view(), name='mission-detail'),
    path('mission/<int:pk>/update/', views.MissionUpdate.as_view(), name='mission-update'),
    path('mission/<int:pk>/delete/', views.MissionDelete.as_view(), name='mission-delete'),
    path('mission/<int:pk>/sessions/', views.SessionListView.as_view(), name='mission-sessions'),
    path('missions/', views.MissionListView.as_view(), name='missions'),
]

urlpatterns += [
    path('partners/', views.PartnerListView.as_view(), name='partners'),
    path('partner/<int:pk>/', views.PartnerDetailView.as_view(), name='partner-detail'),
    path('partner/create/', views.PartnerCreate.as_view(), name='partner-create'),
    path('partner/<int:pk>/update/', views.PartnerUpdate.as_view(), name='partner-update'),
    path('partner/<int:pk>/delete/', views.PartnerDelete.as_view(), name='partner-delete'),
]

urlpatterns += [
    path('parcourss/', views.ParcoursListView.as_view(), name='parcours'),
    path('parcours/<int:pk>/', views.ParcoursDetailView.as_view(), name='parcours-detail'),
    path('parcours/create/', views.ParcoursCreate.as_view(), name='parcours-create'),
    path('parcours/<int:pk>/update/', views.ParcoursUpdate.as_view(), name='parcours-update'),
    path('parcours/<int:pk>/delete/', views.ParcoursDelete.as_view(), name='parcours-delete'),
]

urlpatterns += [
    path('evals/', views.EvalListView.as_view(), name='evals'),
    path('eval/<int:pk>/', views.EvalDetailView.as_view(), name='eval-detail'),
    path('eval/create/', views.EvalCreate.as_view(), name='eval-create'),
    path('eval/<int:pk>/update/', views.EvalUpdate.as_view(), name='eval-update'),
    path('eval/<int:pk>/delete/', views.EvalDelete.as_view(), name='eval-delete'),
]

"""
urlpatterns += [
    path('user/<int:pk_user>/session/<int:pk_session>/<int:pk>/view', views.EvalDetailView.as_view(), name='eval-detail'),
    path('evals/', views.EvalListView.as_view(), name='evals'),
    path('user/<int:pk_user>/session/<int:pk_session>/create/', views.EvalCreate.as_view(), name='eval-create'),
    path('user/<int:pk_user>/session/<int:pk_session>/<int:pk>/update/', views.EvalUpdate.as_view(), name='eval-update'),
    path('user/<int:pk_user>/session/<int:pk_session>/<int:pk>/delete/', views.EvalDelete.as_view(), name='eval-delete'),
    path('user/<int:pk_user>/session/<int:pk_session>/evals/', views.EvalsUserSession.as_view(), name='evals-user-session'),
]
"""
urlpatterns += [
    path('convention/<int:pk>/parcourss', views.ParcoursListView.as_view(), name='convention-parcourss'),
    path('convention/<int:pk>/students', views.UserListView.as_view(), name='convention-students'),
    path('convention/<int:pk>/partners', views.PartnerListView.as_view(), name='convention-partners'),
]