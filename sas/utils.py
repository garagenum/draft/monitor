from django.core import serializers
import json

class ModelUtils():

    def get_fields(self, *args):
        fields = []
        for field in args:
            attr = self._meta.get_field(field)
            #attr = getattr(self, field)
            if attr:
                field_type = attr.get_internal_type()
                field_name = attr.verbose_name
                if field_type == "ManyToManyField":
                    field_value = getattr(self, field).all()#__dir__
                elif field_type == "ForeignKey":
                    field_value = getattr(self, field)
                else:
                    field_value = getattr(self, field)
                fields.append((field_type, field_name, field_value))
        print("===================")
        print("get_fields function returns : ")
        print(fields)
        print("===================")
        return fields

    def get_many_related(self, myfilter, lookup, *args):
        selection = []
        #print(lookup._meta.get_field(filter)['related'])
        my_filter = myfilter + '__id'

        query = lookup.objects.filter(**{my_filter: self.id})

        for related_obj in query:
            related_dict = {
                'id': related_obj.id, 
                'classe': lookup._meta, 
                'filter': myfilter,
                'fields': {}, 
                'obj': related_obj
            }
            for attr in related_obj._meta.get_fields():
                if attr.name in args:
                    related_dict['fields']['field_type'] = attr.get_internal_type()
                    if attr.get_internal_type() == "ManyToManyField":
                        if attr.related_model.hasattr('verbose_name') and hasattr(attr.related_model,'verbose_name'):

                            related_dict['fields'][attr.related_model.verbose_name] = getattr(related_obj, attr.name)
                    else:
                        related_dict['fields'][attr.verbose_name] = getattr(related_obj, attr.name)
            selection.append(related_dict)
        print("selection")
        print(selection)

        return selection


    def get_fk_related(self, filter, lookup, *args):
        selection = []
        my_filter = filter + '__id'

        query = lookup.objects.filter(**{my_filter: self.id})

        for related_obj in query:
            related_dict = {
                'id': related_obj.id, 
                'classe': lookup._meta, 
                'fields': {}, 
                'field_type': "",  # type of field, return by get_internal_type()
                'obj': related_obj
            }
            for attr in related_obj._meta.get_fields():
                if attr.name in args:
                    related_dict['field_type'] = attr.get_internal_type()
                    if attr.get_internal_type() == "ManyToManyField" and hasattr(attr.related_model, 'verbose_name_plural'):
                        related_dict['fields'][attr.related_model.verbose_name_plural] = getattr(related_obj, attr.name)
                    else:
                        related_dict['fields'][attr.verbose_name] = getattr(related_obj, attr.name)
            selection.append(related_dict)
        print("selection")
        print(selection)

        return selection
